import { resolve } from 'path'
import { defineConfig, loadEnv } from 'vite'
import { createVuePlugin } from 'vite-plugin-vue2'
import vueJsx from '@vitejs/plugin-vue-jsx'
import { createSvgIconsPlugin } from 'vite-plugin-svg-icons'
import compressPlugin from 'vite-plugin-compression'
import requireTransform from 'vite-plugin-require-transform'

// compress: 'gzip' | 'brotli' | 'none'
function configCompressPlugin (isBuild, compress) {
  const plugins = []
  if (!isBuild) return plugins
  const compressList = compress.split(',')
  if (compressList.includes('gzip')) {
    plugins.push(
      compressPlugin({
        verbose: true,
        disable: false,
        threshold: 10240,
        algorithm: 'gzip',
        ext: '.gz'
      })
    )
  }
  if (compressList.includes('brotli')) {
    plugins.push(
      compressPlugin({
        verbose: true,
        disable: false,
        threshold: 10240,
        algorithm: 'brotliCompress',
        ext: '.br'
      })
    )
  }
  return plugins
}

export default ({ mode }) => {
  const isBuild = mode === 'production' // mode == production
  const port = loadEnv(mode, process.cwd()).PORT || 9999 // dev port
  return defineConfig({
    base: '/',
    plugins: [
      createVuePlugin(),
      vueJsx(),
      createSvgIconsPlugin({
        iconDirs: [resolve(process.cwd(), 'src/icons/svg')],
        symbolId: 'icon-[dir]-[name]'
      }),
      ...configCompressPlugin(isBuild, 'gzip'),
      requireTransform({ fileRegex: /.js$|.vue$/ })
    ],
    resolve: {
      alias: [
        { find: '@', replacement: resolve(__dirname, 'src') },
        { find: '~@', replacement: resolve(__dirname, 'src') },
        { find: '@api', replacement: resolve(__dirname, 'src/api') },
        { find: '@views', replacement: resolve(__dirname, 'src/views') },
        { find: '@packages', replacement: resolve(__dirname, 'packages') },
        { find: '@components', replacement: resolve(__dirname, 'src/components') },
        { find: '@node_modules', replacement: resolve(__dirname, 'node_modules') },
      ],
      // 导入时，想要省略的扩展名列表-解决extensions后缀报错
      extensions: ['.mjs', '.js', '.ts', '.jsx', '.tsx', '.json', '.vue']
    },
    css: {
      // 置顶传递给CSS预处理器的选项
      preprocessorOptions: {
        less: {
          modifyVars: { // 更改主题在这里
            'primary-color': '#6767CE',
            'link-color': '#199ED8',
            'border-radius-base': '4px',
          },
          javascriptEnabled: true,
          // 自动导入全局样式
          additionalData: ''
        }
      }
    },
    server: {
      port,
      open: false,
      host: '0.0.0.0',
      // 是否开启 https
      https: false,
      hmr: { overlay: false }
    },
    build: {
      outDir: 'dist',
      assetsDir: 'static',
      sourcemap: false,
      chunkSizeWarningLimit: 1000,
      rollupOptions: {
        input: {
          index: resolve(__dirname, "public/index.html")
        },
        output: {
          chunkFileNames: 'static/js/[name]-[hash].js',
          entryFileNames: 'static/js/[name]-[hash].js',
          assetFileNames: 'static/other/[name]-[hash].[ext]',
          manualChunks (id) { // 静态资源分拆打包
            if (id.includes('node_modules')) {
              return id.toString().split('node_modules/')[1].split('/')[0].toString()
            }
          }
        }
      },
      minify: 'terser',
      terserOptions: {
        compress: {
          drop_console: isBuild, // 打包时删除log
          drop_debugger: isBuild, // 打包时删除debugger
          pure_funcs: isBuild ? ['console.log'] : []
        },
        output: {
          comments: isBuild // 去掉注释
        }
      }
    }
  })
}
