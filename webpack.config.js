const path = require('path')
const UglifyJsPlugin = require('uglifyjs-webpack-plugin')
const VueLoaderPlugin = require('vue-loader/lib/plugin')
const HtmlWebpackPlugin = require('html-webpack-plugin')
module.exports = {
  mode: 'development',
  entry: './index.js',
  stats: { children: false },
  output: {
    publicPath: '/dist/',
    path: path.resolve(__dirname, './dist'), // 输出路径dist目录
    filename: 'index.umd.js',
    libraryTarget: 'umd',
    umdNamedDefine: true
  },
  externals: {
    vue: 'vue',
    vuex: 'vuex',
    axios: 'axios'
  },
  module: {
    rules: [
      { test: /\.vue$/, use: 'vue-loader' },
      { test: /\.css$/, use: ['style-loader', 'css-loader'] },
      { test: /\.less$/, use: ['style-loader', 'css-loader', 'less-loader'] },
      // babel的相关配置在.babelrc文件里
      { test: /\.js$/, exclude: /node_modules/, loader: 'babel-loader' },
      { test: /\.(ttf|eot|woff|svg|woff2)/, use: 'file-loader' }
    ]
  },
  optimization: {
    minimizer: [
      new UglifyJsPlugin()
    ]
  },
  plugins: [
    new VueLoaderPlugin(),
    new HtmlWebpackPlugin({
      template: './public/index.html'
    })
  ]
}
