import Vue from 'vue'
// import Vue, { ref, reactive, defineComponent } from 'vue-demi'
import { i18n } from '../packages/locales/index'
import 'ant-design-vue/dist/antd.min.css'
import '../packages/common.less'
import App from './App.vue'
import VueStorage from 'vue-ls'

const storageOptions = {
  namespace: 'kb__', // key prefix
  name: 'ls', // name variable Vue.[ls] or this.[$ls],
  storage: 'local' // storage name session, local, memory
}
Vue.use(VueStorage, storageOptions)
Vue.config.productionTip = false

new Vue({
  i18n,
  created: () => { },
  render: h => h(App)
}).$mount('#app')
