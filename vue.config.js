const path = require('path')
function resolve (dir) {
  return path.join(__dirname, dir)
}
// vue.config.js
module.exports = {
  chainWebpack: (config) => {
    config.resolve.alias.set('@$', resolve('src')).set('@packages', resolve('packages'))
  },
  devServer: {
    port: 7778,
    disableHostCheck: true,
  }
}
