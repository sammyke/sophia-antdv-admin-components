export default {
  data () {
    const _this = this
    return {
      rules: {
        required (msg = '', required = true, type) {
          return [{ required, message: `${type === 'select' ? '请选择' : type === 'upload' ? '请上传' : '请输入'}${msg}` }]
        },
        enter (msg, required = true) {
          return this.required(msg, required)
        },
        select (msg, required = true) {
          return this.required(msg, required, 'select')
        },
        upload (msg, required = true) {
          return this.required(msg, required, 'upload')
        },
        regex (reg, msg, required = true) {
          return [{ required: required, pattern: reg, message: msg }]
        },
        email (msg) {
          return this.enter(msg).concat(_this.rules.regex(/^([.a-zA-Z0-9_-])+@([a-zA-Z0-9_-])+(.[a-zA-Z0-9_-])+/, _this.$t('common.validate.invalidEmail')))
        },
        /**
         * 自定义校验器
         * @param { Function } validator 校验函数, 自动传入Value
         * @param { String } message 错误提示
         * @param { Boolean } required 是否必填项, Default: True, 设置false，则有值的时候校验，无值的时候不校验
         * @returns 返回校验错误结果
         */
        func (validator, message = '', required = true) {
          return [{
            message,
            required,
            validator: (rule, value, callback, source, options) => {
              if (value) {
                const res = validator(rule, value, callback, source, options)
                callback(!res ? message : undefined)
              } else {
                callback(required ? message : undefined)
              }
            }
          }]
        }
      }
    }
  }
}
