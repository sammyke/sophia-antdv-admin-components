import Vue from 'vue'
const TENANT_ID = 'TENANT_ID'
const ACCESS_TOKEN = 'Access-Token'
export default {
  name: '',
  computed: {
    modelAddConfig () { return { title: '添加', name: 'addForm' } },
    modelUpdConfig () { return { title: '修改', name: 'updForm' } },
    modelViewConfig () { return { title: '查看', name: 'viewForm' } },
    tokenHeader () {
      const head = { 'X-Access-Token': Vue.ls.get(ACCESS_TOKEN) }
      const tenantid = Vue.ls.get(TENANT_ID)
      if (tenantid) {
        head['tenant-id'] = tenantid
      }
      return head
    }
  },
  data () {
    return {
      exportLoading: false,
      importLoading: false,
      totalData: 0,
      primaryKey: 'id',
      pageSize: 10,
      // 设置true，弹出确认删除的弹窗
      confirmDelete: false,
      apiModel: {
        search: (parameter) => { },
        add: (parameter) => { },
        upd: (parameter) => { },
        del: (parameter) => { }
      },
      labelCol: {
        xs: { span: 24 }, // <576px
        sm: { span: 12 }, // ≥576px
        md: { span: 8 }, // ≥768px
        lg: { span: 6 }, // ≥992px
        xl: { span: 4 }, // ≥1200px
        xxl: { span: 4 } // ≥1600px
      },
      toggleSearchBar: false,
      // 查询条件参数
      filter: {},
      query: {},
      loadData: (params) => {
        var parameter = Object.assign(this.query, params)
        for (var o in parameter) {
          if (parameter[o] === 0 || parameter[o] === '0' || parameter[o] === false) {
            continue
          }
          if (!parameter[o]) {
            delete parameter[o]
          }
        }

        const search = this.apiModel.search(parameter)
        search.then(res => {
          this.totalData = res.result.total
        })
        return search
      },
      // 表头
      // columns: [],
      modelFormVisible: false,
      modelAddVisible: false,
      modelUpdVisible: false,
      modelViewVisible: false,
      deleteModalVisible: false,
      JSON: {
        parse (val) {
          return val ? JSON.parse(val) : val
        },
        string (val) {
          return val ? JSON.stringify(val) : val
        }
      }
    }
  },
  created () {
    this.initQueryParameters()
  },
  methods: {
    clearForm () {
      this.clearCustom && this.clearCustom()
      this.$refs.formModel.reset()
      this.$refs.formModel.clearImage && this.$refs.formModel.clearImage()
      this.$refs.formModel.clearCustom && this.$refs.formModel.clearCustom()
      this.$refs.formModel.clearLangPack && this.$refs.formModel.clearLangPack()
    },
    clearFormAdd () {
      this.clearCustom && this.clearCustom()
      this.$refs.formAdd.reset()
      this.$refs.formAdd.clearImage && this.$refs.formAdd.clearImage()
      this.$refs.formAdd.clearCustom && this.$refs.formAdd.clearCustom()
      this.$refs.formAdd.clearLangPack && this.$refs.formAdd.clearLangPack()
    },
    clearFormUpd () {
      this.clearCustom && this.clearCustom()
      this.$refs.formUpd.reset()
      this.$refs.formUpd.clearImage && this.$refs.formUpd.clearImage()
      this.$refs.formUpd.clearCustom && this.$refs.formUpd.clearCustom()
      this.$refs.formUpd.clearLangPack && this.$refs.formUpd.clearLangPack()
    },
    /**
     * 初始化查询参数
     */
    initQueryParameters () {
      this.query = {}
    },
    deleteQueryParams (name) {
      if (name) {
        const newQuery = JSON.parse(JSON.stringify(this.$route.query))
        if (newQuery[name]) {
          delete newQuery[name]
          this.$nextTick(() => {
            this.$bus.$emit('closeCurTab')
          })
          this.$router.replace({ query: newQuery })
          delete this.filter[name]
          this.resetFilter()
        }
      }
    },
    handleToggleSearchBar () {
      this.toggleSearchBar = !this.toggleSearchBar
    },
    refresh (flag) {
      this.beforeRefresh && this.beforeRefresh()
      const table = (this.$refs.dataTable || this.$refs.table)
      table && table.refresh(flag)
      table && table.clearSelectedRows()
    },
    beforeHandleTableSearch () { },
    handleTableSearch () {
      if (this.beforeHandleTableSearch() !== false) {
        this.refresh(true)
      }
    },
    handleFilterCancel (filter) {
      this.filter = {}
      this.$nextTick(() => {
        this.filter = filter
        this.filterCancel(filter)
      })
    },
    resetFilter () {
      const filter = this.filter
      this.filter = {}
      this.$nextTick(() => {
        this.filter = filter
      })
    },
    filterCancel (filter) { },
    handleFilterBtnReset () {
      this.$refs.table && this.$refs.table.handleFilterReset(true)
    },
    // 新表格组件重置按钮
    handleFilterReset (filter) {
      this.filter = {}
      this.$nextTick(() => {
        this.filter = filter
        this.clearParams(filter)
        this.refresh(true)
      })
    },
    // 表格页面需要重写该方法，才能清理特定组件，如日期区间空间的v-model的值，避免清不掉参数
    clearParams (filter) { },
    handleClearParams (filter) {
      this.filter = {}
      this.$nextTick(() => {
        this.filter = filter
        this.clearParams(filter)
        this.refresh(true)
      })
    },
    initAddData (formData) { return formData },
    handleModelAdd () {
      if (this.$refs.formModel) {
        this.modelFormVisible = true
      } else {
        this.modelAddVisible = true
      }
      this.$nextTick(() => {
        const initData = this.initAddData({})
        const model = (this.$refs.formAdd || this.$refs.formModel)
        model.initData(initData)
      })
    },
    initUpdData (formData) { return formData },
    handleModelUpd (record) {
      if (this.$refs.formModel) {
        this.modelFormVisible = true
      } else {
        this.modelUpdVisible = true
      }
      this.$nextTick(() => {
        const formData = Object.assign({}, record)
        const initData = this.initUpdData(formData)
        const model = (this.$refs.formUpd || this.$refs.formModel)
        model.initData(initData)
      })
    },
    handleModelView (record) {
      this.modelViewVisible = true
      this.$nextTick(() => {
        this.$refs.formView.initData(record[this.primaryKey])
      })
    },
    handleModelSubmit (formData) {
      if (this[`beforeHandle${!formData.id ? 'Add' : 'Upd'}`](formData) !== false) {
        this.$bus.$emit('handleDrawerSubmit')
        this.apiModel[!formData.id ? 'add' : 'upd'](formData).then(res => {
          this.$bus.$emit('handleDrawerSubmit', false)
          if (res && res.success) {
            this.clearForm()
            this.modelFormVisible = false
            this[`afterHandle${!formData.id ? 'Add' : 'Upd'}ed`](formData, res)
            this.$message.success('操作成功')
            this.refresh(true)
          } else {
            this.$bus.$emit('handleDrawerSubmit', false)
            this.$message.error(res.message || '操作错误')
          }
        }).catch(e => {
          console.log(e.message)
          this.$bus.$emit('handleDrawerSubmit', false)
        })
      }
    },
    beforeHandleAdd (formData) { },
    afterHandleAdded (formData, resData) { },
    handleModelUpdSubmit (formData) {
      if (this.beforeHandleUpd(formData) !== false) {
        this.$bus.$emit('handleDrawerSubmit')
        this.apiModel.upd(formData).then(res => {
          this.$bus.$emit('handleDrawerSubmit', false)
          if (res && res.success) {
            this.modelUpdVisible = false
            this.clearFormUpd()
            this.afterHandleUpded(formData, res)
            this.$message.success('操作成功')
            this.refresh(true)
          } else {
            this.$bus.$emit('handleDrawerSubmit', false)
            this.$message.error(res.message || '操作失败')
          }
        }).catch(e => {
          console.log(e.message)
          this.$bus.$emit('handleDrawerSubmit', false)
        })
      }
    },
    beforeHandleUpd (formData) { },
    afterHandleUpded (formData, resData) { },
    afterModelAddClose () { },
    handleModelAddClose (flag = true) {
      if (flag) {
        this.afterModelAddClose()
      }
      this.modelAddVisible = false
    },
    afterModelUpdClose () { },
    handleModelUpdClose (flag = true) {
      if (flag) {
        this.afterModelUpdClose()
      }
      this.modelUpdVisible = false
    },
    handleModelViewClose () {
      this.modelViewVisible = false
    },
    beforeHandleDel (record) {
      return true
    },
    handleModelDel (record) {
      if (this.confirmDelete) {
        this.tempRecord = record
        this.deleteModalVisible = true
      } else {
        if (this.beforeHandleDel(record) && record[this.primaryKey] !== undefined) {
          return new Promise((resolve, reject) => {
            this.apiModel.del(record[this.primaryKey]).then(res => {
              if (res && res.success) {
                resolve(res)
                this.$message.success('操作成功')
                this.refresh(true)
                this.afterHandleDel(record)
              } else {
                reject(res.message)
                this.$message.error(res.message || '操作失败')
              }
            }).catch(e => {
              reject(e.message)
            })
          })
        }
      }
    },
    handleDeleteOk (res) {
      if (res === 'ok') {
        this.apiModel.del(this.tempRecord[this.primaryKey]).then(res => {
          if (res && res.success) {
            this.$message.success('操作成功')
            this.refresh(true)
            this.deleteModalVisible = false
            this.afterHandleDel(this.tempRecord)
          }
        })
      }
    },
    // 确认删除输入后点击ok事件
    handleConfirmDeleteOk (res) {
      if (res === 'ok') {
        this.apiModel.del(this.tempRecord[this.primaryKey]).then(res => {
          if (res && res.success) {
            this.$message.success('操作成功')
            this.refresh(true)
            this.deleteModalVisible = false
            this.afterHandleDel(this.tempRecord)
          }
        })
      }
    },
    afterHandleDel (record) { },
    // 批量删除
    batchDel: function () {
      if (!this.apiModel.batchDel) {
        return this.$message.error('请设置apiModel.batchDel方法!')
      }
      if (this.selectedRowKeys.length <= 0) {
        return this.$message.warning('请选择一条记录!')
      } else {
        const ids = this.selectedRowKeys.join(',')
        var that = this
        this.$confirm({
          title: '确认删除',
          content: '是否删除选中记录?',
          onOk: function () {
            that.loading = true
            that.apiModel.batchDel(ids).then((res) => {
              if (res.success) {
                // 重新计算分页问题
                that.$message.success('操作成功')
                that.refresh()
              } else {
                that.$message.warning(res.message)
              }
            }).finally(() => {
              that.loading = false
            })
          }
        })
      }
    },
    /* 导出 */
    handleExportXls2 () {
      const paramsStr = encodeURI(JSON.stringify(this.filter))
      const url = `${window._CONFIG['domianURL']}/${this.exportXlsUrl}?paramsStr=${paramsStr}`
      window.location.href = url
    },
    async handleExportXls (fileName) {
      if (!fileName || typeof fileName !== 'string') {
        fileName = '导出文件'
      }
      const param = this.filter
      if (this.selectedRowKeys && this.selectedRowKeys.length > 0) {
        param['selections'] = this.selectedRowKeys.join(',')
      }
      param.fileName = fileName
      console.log('导出参数', param)
      this.exportLoading = true
      await Vue.prototype.$axios.$download(url, parameter)(this.exportXlsUrl, param)
      this.exportLoading = false
    },
    /* 导入 */
    handleImportExcel (info) {
      this.loading = true
      if (info.file.status !== 'uploading') {
        console.log(info.file, info.fileList)
      }
      if (info.file.status === 'done') {
        this.loading = false
        if (info.file.response.success) {
          // this.$message.success(`${info.file.name} 文件上传成功`);
          if (info.file.response.code === 201) {
            const { message, result: { msg, fileUrl, fileName } } = info.file.response
            const href = window._CONFIG['domianURL'] + fileUrl
            window.open(href)
            // this.$warning({
            //   title: message,
            //   content: (<div>
            //     <span>{msg}</span><br />
            //     <span>具体详情请 <a href={href} target="_blank" download={fileName}>点击下载</a> </span>
            //   </div>
            //   )
            // })
          } else {
            this.$message.success(info.file.response.message || `${info.file.name} 文件上传成功`)
          }
          this.refresh(true)
        } else {
          this.$message.error(`${info.file.name} ${info.file.response.message}.`)
        }
      } else if (info.file.status === 'error') {
        this.loading = false
        if (info.file.response.status === 500) {
          const data = info.file.response
          const token = Vue.ls.get(ACCESS_TOKEN)
          if (token && data.message.includes('Token失效')) {
            this.$error({
              title: '登录已过期',
              content: '很抱歉，登录已过期，请重新登录',
              okText: '重新登录',
              mask: false,
              onOk: () => {
                Vue.prototype.$store.dispatch('Logout').then(() => {
                  Vue.ls.remove(ACCESS_TOKEN)
                  window.location.reload()
                })
              }
            })
          }
        } else {
          this.$message.error(`文件上传失败: ${info.file.msg} `)
        }
      }
    }
  }
}
