import validatorRules from './validator-rules'
const form = {
  mixins: [validatorRules],
  model: {
    prop: 'visible'
  },
  provide () {
    return {
      form: () => { return this.form }
    }
  },
  props: {
    visible: Boolean,
    config: {
      type: Object,
      default: () => {
        return {
          title: String,
          name: String
        }
      }
    }
  },
  data () {
    return {
      modelVisible: this.visible,
      labelColRow: { xs: { span: 7 }, sm: { span: 6 } },
      wrapperColRow: { xs: { span: 17 }, sm: { span: 17, offset: 1 } },
      labelCol: { xs: { span: 6, offset: 2 }, sm: { span: 6, offset: 2 } },
      wrapperCol: { xs: { span: 14, offset: 2 }, sm: { span: 14, offset: 2 } },
      labelCol2: { xs: { span: 6 }, sm: { span: 3, offset: 1 } },
      wrapperColFull: { xs: { span: 24 }, sm: { span: 23, offset: 1 } },
      autoSize: { minRows: 2, maxRows: 4 },
      autoSize3: { minRows: 3, maxRows: 4 },
      autoSize4: { minRows: 4, maxRows: 20 },
      autoSize8: { minRows: 8, maxRows: 16 },
      autoSize27: { minRows: 2, maxRows: 27 },
      // 表单
      form: {},
      // 用户提交按钮的loading处理
      isSubmit: false,
      // 是否以Promise返回
      isPromise: false
    }
  },
  created () {
    this.form = this.$form.createForm(this, {
      name: this.config.name,
      onFieldsChange: (props, changedFields) => this.onFieldsChange(props, changedFields)
    })
  },
  methods: {
    reset () {
      this.form.resetFields()
    },
    onFieldsChange (props, changedFields) {
      // if (Object.keys(props.fieldsStore.fields).some(key => props.fieldsStore.fields[key].touched)) {
      //   this.$bus.$emit('handleDrawerEnable', false)
      // } else {
      //   this.$bus.$emit('handleDrawerEnable', true)
      // }
    },
    clearLangPack () {
      if (this.$refs.nameLangPack) {
        this.$refs.nameLangPack.clear()
      }
      if (this.$refs.descLangPack) {
        this.$refs.descLangPack.clear()
      }
      if (this.$refs.valueLangPack) {
        this.$refs.valueLangPack.clear()
      }
    },
    initData (record) {
      if (this.beforeInitData(record) !== false) {
        const form = document.getElementsByClassName('form-height-limit')
        if (form && form.length) {
          form[0].scrollTop = 0
        }
        this.$nextTick(() => {
          // 设置初始值
          this.form.setFieldsValue(record)
        })
      }
    },
    /* 解决需要处理form数据的问题 */
    beforeInitData (record) { },
    handleCancel (flag = true) {
      this.modelVisible = false
      this.reset()
      this.$emit('input')
      this.$emit('close', flag)
      this.afterCancel()
      this.clearLangPack()
    },
    afterCancel () {
      this.clearImage && this.clearImage()
      this.clearCustom && this.clearCustom()
      this.clearLangPack && this.clearLangPack()
    },
    beforeValidate () { },
    handleSubmit () {
      this.beforeValidate()
      if (this.isPromise === true) {
        return new Promise((resolve, reject) => {
          this.form.validateFieldsAndScroll((err, fieldsData) => {
            if (!err) {
              for (const o in fieldsData) {
                if (fieldsData[o] === undefined || fieldsData[o] === null) {
                  delete fieldsData[o]
                  continue
                } else if (typeof fieldsData[o] === 'string') {
                  fieldsData[o] = fieldsData[o].trim()
                }
              }
              resolve(fieldsData)
              // this.clearLangPack()
              this.afterSubmit(fieldsData)
            } else {
              reject(err)
            }
          })
        })
      } else {
        this.form.validateFieldsAndScroll((err, fieldsData) => {
          if (!err) {
            for (var o in fieldsData) {
              if (fieldsData[o] === undefined || fieldsData[o] === null) {
                fieldsData[o] = ''
              } else if (typeof fieldsData[o] === 'string') {
                fieldsData[o] = fieldsData[o].trim()
              }
            }
            const callbackData = this.beforeSubmit(fieldsData)
            if (callbackData instanceof Promise) {
              callbackData.then(noErr => {
                if (noErr) {
                  this.$emit('submit', fieldsData)
                  // this.clearLangPack()
                  this.afterSubmit(fieldsData)
                }
              })
            } else if (callbackData !== false && typeof callbackData === 'object') {
              this.$emit('submit', callbackData)
              // this.clearLangPack()
              this.afterSubmit(callbackData)
            }
          }
        })
      }
    },
    beforeSubmit (formData) { },
    afterSubmit (formData) { }
  },
  watch: {
    visible (newVal) {
      this.modelVisible = newVal
    }
  }
}
if (!window.__POWERED_BY_QIANKUN__) {
  form.inject = ['closeCurrent']
}
export default form
