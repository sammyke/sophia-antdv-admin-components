import Vue from 'vue'
// import Vue, { ref, reactive, defineComponent } from 'vue-demi'
import VueI18n from 'vue-i18n'
import enEN from './en.json'
import zhCN from './zh-cn.json'

Vue.use(VueI18n)
const i18n = new VueI18n({
  locale: 'zhCN',
  messages: { enEN, zhCN }
})
i18n.missing = (locale, key, vm) => {
  // 处理翻译缺失
  console.error(`Locale: ${locale}, key: ${key}`)
}
export {
  enEN,
  zhCN,
  i18n
}
