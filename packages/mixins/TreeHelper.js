export default {
  methods: {
    listToTree (list, treeData, pid, options) {
      options = options || {}
      const checkedKeys = options.checkedKeys || []
      const disableKey = options.disableKey || undefined
      const prefix = options.prefix
      const suffix = options.suffix
      const split = options.split
      list.forEach(item => {
        // 判断是否为父级菜单
        const itemPid = item[options.pid || 'pid']
        const id = item[options.id || 'id']
        const name = item[options.name || 'name']
        if (item && (itemPid || itemPid === 0 || itemPid === null || itemPid === '') && (pid || pid === 0 || pid === null || pid === '') && itemPid === pid) {
          let title = ''
          if (prefix) {
            title += item[prefix]
          }
          if (split === 'brackets') {
            title = `(${title}) - `
          }
          title += name || ''
          if (suffix) {
            let suffixStr = item[suffix]
            if (split === 'brackets') {
              suffixStr = ` - (${suffixStr})`
            }
            title += suffixStr
          }
          const child = {
            key: id,
            value: id,
            title,
            disabled: item.disabled || disableKey === id || disableKey === itemPid,
            pid: itemPid,
            children: []
          }
          // child.scopedSlots = { icon: 'customIcon', title: 'title' }
          // 迭代 list， 找到当前菜单相符合的所有子菜单
          const optionParams = Object.assign(options, checkedKeys, { disableKey })
          if (disableKey === id || disableKey === itemPid || item.disabled) {
            optionParams.disableKey = id
          }
          this.listToTree(list, child.children, id, optionParams)
          // 删掉不存在 children 值的属性
          if (child.children.length <= 0) {
            child.scopedSlots = { icon: 'customIcon', title: 'title' }
            delete child.children
          } else if (checkedKeys.indexOf(id) > -1) {
            checkedKeys.splice(checkedKeys.indexOf(id), 1)
          }
          // 加入到树中
          treeData.push(child)
        }
        if (treeData && treeData.length) {
          treeData.sort((a, b) => {
            return a.id - b.id
          })
        }
      })
    }
  }
}
