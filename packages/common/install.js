import Vue from 'vue'
import App from './App.vue'
import Antd from 'ant-design-vue'
import VueRouter from 'vue-router'
import Api from '@/base/api/index'
import store from '@/base/store/index'
import routes from '@/base/router/index'
import { install as SophiaInstaller } from 'sophia-admin-components'

import 'ant-design-vue/dist/antd.css'
import 'sophia-admin-components/packages/common.less'

Vue.use(Antd)
Vue.use(VueRouter)
Vue.use(SophiaInstaller)
Vue.config.productionTip = false

let router = null
let instance = null
function render (props = {}) {
  const { container, routerBase, main } = props
  router = new VueRouter({
    base: window.__POWERED_BY_QIANKUN__ ? routerBase : '/base',
    mode: 'history',
    routes
  })
  // debugger
  if (main) {
    Vue.prototype.$main = main
    Vue.prototype.$axios = main.$axios
    Vue.prototype.$store = main.$store
    Vue.prototype.$ls = main.$ls
    Vue.prototype.$confirm = main.$confirm
    Vue.prototype.$notification = main.$notification
    Vue.prototype.$info = main.$info
    Vue.prototype.$success = main.$success
    Vue.prototype.$error = main.$error
    Vue.prototype.$warning = main.$warning
    Vue.prototype.$message = main.$message
    Api.setApi(main.$api)
  }
  instance = new Vue({
    router,
    store,
    i18n: main.$i18n,
    render: (h) => h(App)
  }).$mount(container ? container.querySelector('#tms-base') : '#tms-base')
}

// 独立运行时
if (!window.__POWERED_BY_QIANKUN__) {
  console.log('独立运行')
  render()
}

if (window.__POWERED_BY_QIANKUN__) {
  // eslint-disable-next-line
  __webpack_public_path__ = window.__INJECTED_PUBLIC_PATH_BY_QIANKUN__
  console.log('qiankun', __webpack_public_path__)
}

export async function bootstrap () {
  console.log('[vue] vue app bootstraped')
}
export async function mount (props) {
  render(props)
}
export async function unmount () {
  instance.$destroy()
  instance.$el.innerHTML = ''
  instance = null
  router = null
}
