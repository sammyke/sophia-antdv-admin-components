import moment from 'moment'
// window.moment = moment
export default {
  methods: {
    renderNone (val) {
      return val !== undefined && val !== null ? val : ''
    },
    renderEmpty (val) {
      return val !== undefined && val !== null ? val : '无'
    },
    renderNumber (val) {
      return !isNaN(val) ? Number(val) : 0
    },
    renderTotal (val, record) {
      // if (record.totalColumn) {
      //   console.log(Object.assign({}, record))
      // }
      return record.totalColumn ? <span class="red">{this.renderNumber(val)}</span> : val
    },
    getMomentTime () {
      return [moment('00:00:00', 'HH:mm:ss'), moment('23:59:59', 'HH:mm:ss')]
    },
    renderMomentTime (val = new Date()) {
      return moment(val, 'YYYY-MM-DD HH:mm:ss')
    },
    renderMinuteTime (val) {
      return this.renderDate(val, 'YYYY-MM-DD HH:mm')
    },
    renderMomentDay (val) {
      return moment(val, 'YYYY-MM-DD')
    },
    renderDateDay (val) {
      return this.renderDate(val, 'YYYY-MM-DD')
    },
    renderDate (val, format) {
      // console.log(moment.tz.guess())
      // moment.tz.setDefault(moment.tz.guess())
      format = typeof format === 'string' ? format : 'YYYY-MM-DD HH:mm:ss'
      return val ? moment(val, format).format(format) : this.renderEmpty(val)
    },
    renderDateFormat (val, format) {
      // console.log(moment.tz.guess())
      // moment.tz.setDefault(moment.tz.guess())
      format = typeof format === 'string' ? format : 'YYYY-MM-DD HH:mm'
      return val ? moment(val, format).format(format) : this.renderEmpty(val)
    },
    renderTime (val) {
      return val ? moment(val, 'HH:mm:ss') : this.renderEmpty(val)
    },
    renderLink (val, record, rowIndex) {
      return val ? <a href="javascript:;" onclick={(e) => { e.preventDefault(); this.handleLinkClick(val, record, rowIndex) }}>{val}</a> : '无'
    },
    renderTag (val, record, rowIndex) {
      return <a-tag style="min-width:50px;text-align:center;" color="blue" onclick={() => { this.handleTagClick && this.handleTagClick(val, record, rowIndex) }}>{val}</a-tag>
    },
    renderTagList (val) {
      var tags
      try {
        tags = JSON.parse(val)
        if (typeof tags === 'string') {
          tags = tags.split(',')
        }
        if (tags && tags.length) {
          var res = []
          for (let i = 0; i < tags.length; i++) {
            var tag = tags[i]
            if (tag.trim()) {
              res.push(
                <a-tag color="blue">{tag}</a-tag>
              )
            }
          }
          return res
        }
      } catch (error) {
        console.log(error.message)
        return tags
      }
    },
    renderThumb (src) {
      // if (!src) return this.renderEmpty(src)
      // const imageData = src.split('/')
      // if (imageData.length && imageData.length >= 3) {
      //   imageData[3] = 'thumbnail/' + imageData[3] + '/n7'
      //   src = imageData.join('/')
      // }
      return <aosom-image url={src} level={7} />
    },
    renderYesNo (val = false) {
      return this.renderCustom(String(Number(val)), this.global.dict.yesNo)
    },
    renderRightWrong (val = false) {
      return String(Number(val)) === '1' ? <icon-font type="icon-right" class="icon-right" /> : <icon-font type="icon-wrong" class="icon-wrong" />
    },
    renderEnable (val) {
      return this.renderCustom(val, this.global.dict.enableStatus)
    },
    renderCustom (val, dataSource, option) {
      option = option || { key: 'value', value: 'label' }
      let ds = []
      if (typeof dataSource === 'string') {
        if (dataSource && this[dataSource] !== undefined && this[dataSource].length) {
          [...ds] = this[dataSource]
        }
      } else if (Array.isArray(dataSource) && dataSource.length) {
        [...ds] = dataSource
      }
      const node = ds.find(data => data[option.key] === (/\d+/.test(val) && typeof val === 'number' ? Number(val) : typeof val === 'string' ? val : val))
      return node ? node[option.value] : this.renderNone(val)
    },
    renderCurrency (val) {
      return this.renderCustom(val, this.global.dict.currency)
    },
    // 金额显示小数点后两位
    renderDotAmount (val) {
      return this.$toFixed(val, 2, 'stirng')
    },
    // 汇率显示小数点后四位
    renderExchangeRate (val) {
      return this.$toFixed(val, 4, 'string')
    },
    renderAmount (val) {
      return this.$currency(val, false)
    }
  }
}
