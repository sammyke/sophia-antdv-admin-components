import * as Components from './packages/index'
// i18n
import * as locales from './packages/locales/index'
const install = function (app, options = {}) {
  // 判断是否安装
  if (install.installed) return
  // 遍历注册全局组件
  let component
  Object.keys(Components).map(key => {
    component = Components[key]
    app.component(key, component)
  })
}

// 判断是否是直接引入文件
if (typeof window !== 'undefined' && window.Vue) {
  install(window.Vue)
}
// 导出的对象必须具有 install，才能被 Vue.use() 方法安装
export {
  install,
  locales,
  Components
}
