# sophia-admin-components
Ant Design Vue 控制台组件库
默认使用small样式，更简洁
- 布局组件
  -  表格组件 Table
  -  表单项 FormItem
  -  列 Col
  -  弹窗组件 Modal
  -  抽屉面板 Drawer
  -  搜索树 SearchTree
  -  收缩框 Collapse
  -  更多(Hover) MoreLink
  -  下拉按钮 DropdownButton
- 表单组件
  -  级联组件 Cascader
  -  日期选择器 DatePicker
  -  数字输入框 Number
  -  下拉选择器 Select
- 业务组件
  -  删除确认对话框 DeleteModal 删除弹出包含"请输入DELETE"的弹窗
  -  文件上传组件 FileUploader 支持图片、视频、文档、自定义格式，预览，自定义实现等
  -  图片组件 Image 图片组件，可显示在列表页，支持点击预览
  -  上传组件 Uploader

### Project setup

```
yarn install
```

### Compiles and hot-reloads for development

```
yarn serve
```

### Compiles and minifies for production

```
yarn build
```

### Lints and fixes files

```
yarn lint
```

### Customize configuration

See [Configuration Reference](https://cli.vuejs.org/config/).
